package com.kalkotekedar.www.weatherforecast.view.activity.weather;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import com.kalkotekedar.www.weatherforecast.BaseActivity;
import com.kalkotekedar.www.weatherforecast.R;
import com.kalkotekedar.www.weatherforecast.model.City;
import com.kalkotekedar.www.weatherforecast.model.List;
import com.kalkotekedar.www.weatherforecast.presenter.search.IWeatherListPresenter;
import com.kalkotekedar.www.weatherforecast.presenter.search.WeatherListPresenter;
import com.kalkotekedar.www.weatherforecast.utils.Constants;
import com.kalkotekedar.www.weatherforecast.view.Adapter.ForecastAdapter;

public class WeatherList extends BaseActivity implements IWeatherList {

    private Context context = this;
    private ForecastAdapter adapter;
    private RecyclerView RV_displayForecast;
    private IWeatherListPresenter iWeatherListPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather_list);
        init();
        Bundle bundle = getIntent().getExtras();
        if (bundle.getString(Constants.CITY) != null) {
            iWeatherListPresenter.getForecastByCity(bundle.getString(Constants.CITY));
        }
    }

    private void init() {
        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mToolbar.setTitle("Forecast By City");
        mToolbar.setTitleTextColor(getResources().getColor(R.color.white));
        setSupportActionBar(mToolbar);
        iWeatherListPresenter = new WeatherListPresenter(this, context);

        LinearLayoutManager layoutManager = new LinearLayoutManager(context);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        RV_displayForecast = (RecyclerView) findViewById(R.id.rv_forecast);
        RV_displayForecast.setLayoutManager(layoutManager);
    }

    @Override
    public void displayForecast(java.util.List<List> list, City city) {
        adapter = new ForecastAdapter(context, list);
        RV_displayForecast.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        TextView citName = (TextView) findViewById(R.id.tv_city);
        citName.setText(city.getName() + ", " + city.getCountry());
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (adapter != null) {
            adapter.notifyDataSetChanged();
        }
    }
}