package com.kalkotekedar.www.weatherforecast.presenter.search;

/**
 * Created by Kedar on 28-06-2016.
 */
public interface IWeatherListPresenter {
    void getForecastByCity(String city);
}
