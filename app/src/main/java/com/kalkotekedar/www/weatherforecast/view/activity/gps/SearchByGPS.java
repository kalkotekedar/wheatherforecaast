package com.kalkotekedar.www.weatherforecast.view.activity.gps;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.TextView;

import com.kalkotekedar.www.weatherforecast.BaseActivity;
import com.kalkotekedar.www.weatherforecast.R;
import com.kalkotekedar.www.weatherforecast.model.City;
import com.kalkotekedar.www.weatherforecast.model.List;
import com.kalkotekedar.www.weatherforecast.presenter.gps.GPSPresenter;
import com.kalkotekedar.www.weatherforecast.presenter.gps.IGPSPresenter;
import com.kalkotekedar.www.weatherforecast.view.Adapter.ForecastAdapter;

public class SearchByGPS extends BaseActivity implements ISearchByGPS, LocationListener {

    private Context context = this;
    private IGPSPresenter igpsPresenter;
    private RecyclerView RV_displayForecast;
    private String currentCity;
    private static final int TIME = 60 * 1000;
    private static final int DISTANCE = 1000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_by_gps);

        if (Build.VERSION.SDK_INT == Build.VERSION_CODES.M) {
            loadPermissions(Manifest.permission.ACCESS_FINE_LOCATION, REQUEST_FINE_LOCATION);
            checkGPSEnabled();
        } else
            checkGPSEnabled();

        init();
    }

    private void init() {
        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mToolbar.setTitle("Weather by GPS");
        mToolbar.setTitleTextColor(getResources().getColor(R.color.white));
        setSupportActionBar(mToolbar);
        igpsPresenter = new GPSPresenter(this, context);

        LinearLayoutManager layoutManager = new LinearLayoutManager(context);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        RV_displayForecast = (RecyclerView) findViewById(R.id.rv_forecast);
        RV_displayForecast.setLayoutManager(layoutManager);

        LocationManager mLocationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (Build.VERSION.SDK_INT == Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED)
                mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, TIME, DISTANCE, this);
        } else
            mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, TIME, DISTANCE, this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (Build.VERSION.SDK_INT == Build.VERSION_CODES.M) {
            loadPermissions(Manifest.permission.ACCESS_FINE_LOCATION, REQUEST_FINE_LOCATION);
            checkGPSEnabled();
        } else
            checkGPSEnabled();
    }

    @Override
    public void displayForecast(java.util.List<List> foreLists, City city) {
        ForecastAdapter adapter = new ForecastAdapter(context, foreLists);
        RV_displayForecast.setAdapter(adapter);
        TextView cityName = (TextView) findViewById(R.id.tv_city);
        cityName.setText(city.getName() + ", " + city.getCountry());
    }

    @Override
    public void onLocationChanged(Location location) {
        Log.e("KK", "onLocationChanged: " + location);
        igpsPresenter.getCityName(context, location);
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        Log.e("KK", "onStatusChanged: " + status);
    }

    @Override
    public void onProviderEnabled(String provider) {
    }

    @Override
    public void onProviderDisabled(String provider) {
    }
}