package com.kalkotekedar.www.weatherforecast.view.activity.gps;


import com.kalkotekedar.www.weatherforecast.model.City;
import com.kalkotekedar.www.weatherforecast.model.List;

/**
 * Created by Kedar on 01-07-2016.
 */
public interface ISearchByGPS {
    void displayForecast(java.util.List<List> foreLists, City city);
}
