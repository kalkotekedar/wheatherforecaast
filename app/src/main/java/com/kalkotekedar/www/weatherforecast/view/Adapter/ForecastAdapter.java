package com.kalkotekedar.www.weatherforecast.view.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kalkotekedar.www.weatherforecast.R;
import com.kalkotekedar.www.weatherforecast.model.List;
import com.kalkotekedar.www.weatherforecast.model.Weather;

import java.util.Date;


/**
 * Created by Kedar on 28-06-2016.
 */
public class ForecastAdapter extends RecyclerView.Adapter<ForeCastItem> {

    private Context context;
    private java.util.List<List> list;

    public ForecastAdapter(Context context, java.util.List<List> list) {
        Log.i("KK", "ForecastAdapter: " + list.toString());
        this.context = context;
        this.list = list;
    }

    @Override
    public ForeCastItem onCreateViewHolder(ViewGroup parent, int viewType) {
        View viewHolder = LayoutInflater.from(context).inflate(R.layout.item_forecast_view, parent, false);
        return new ForeCastItem(viewHolder);
    }

    @Override
    public void onBindViewHolder(ForeCastItem holder, int position) {
        List tempList = list.get(position);
        Log.i("KK", "onBindViewHolder: " + tempList.toString());

        holder.timeStamp.setText(new Date(tempList.getDt() * 1000) + "");

        holder.day.setText(tempList.getTemp().getDay() + "K");
        holder.night.setText(tempList.getTemp().getNight() + "K");
        holder.min.setText(tempList.getTemp().getMin() + "K");
        holder.max.setText(tempList.getTemp().getMax() + "K");
        holder.morn.setText(tempList.getTemp().getMorn() + "K");
        holder.eve.setText(tempList.getTemp().getEve() + "K");

        holder.pressure.setText(tempList.getPressure() + "hPa");
        holder.humidity.setText(tempList.getHumidity() + "%");
        holder.speed.setText(tempList.getSpeed() + "m/sec");
        holder.deg.setText(tempList.getDeg() + "");
        holder.cloud.setText(tempList.getClouds() + "%");
        holder.rain.setText(tempList.getRain() + "%");

        java.util.List<Weather> weatherList = tempList.getWeather();
        Weather weather = weatherList.get(0);
        holder.main.setText(weather.getMain());
        holder.description.setText(weather.getDescription());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}