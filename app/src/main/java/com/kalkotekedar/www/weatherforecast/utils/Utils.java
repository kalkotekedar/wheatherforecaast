package com.kalkotekedar.www.weatherforecast.utils;

import android.support.design.widget.Snackbar;
import android.view.View;

/**
 * Created by Kedar on 29-06-2016.
 */
public class Utils {

    public static void showSnackbar(View view, String message, int duration) {
        Snackbar.make(view, message, duration)
                .setAction("OK", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                    }
                })
                .setActionTextColor(view.getResources().getColor(android.R.color.holo_green_dark))
                .show();
    }
}
