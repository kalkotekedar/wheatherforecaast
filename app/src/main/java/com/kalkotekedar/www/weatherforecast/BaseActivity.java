package com.kalkotekedar.www.weatherforecast;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;

/**
 * Created by Kedar on 29-06-2016.
 */
public class BaseActivity extends AppCompatActivity {

    private Context context = this;
    public static final int REQUEST_FINE_LOCATION = 0;

    public boolean checkInternetConnection() {
        ConnectivityManager connectivityManager =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnectedOrConnecting();
    }

    /**
     * Check the GPS is on or not if not request to switch on GPS
     */
    public void checkGPSEnabled() {
        LocationManager mLocationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        Log.i("KK", "checkGPSEnabled: " + mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER));
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT) {
            if (!mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                switchOnGPS();
            }
        } else {
            String providers = Settings.Secure.getString(context.getContentResolver(),
                    Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
            if (TextUtils.isEmpty(providers)) {
                switchOnGPS();
            }
        }
    }

    /**
     * TO get the permission in  marshmallow ask runtime permission
     */
    @TargetApi(Build.VERSION_CODES.M)
    public void loadPermissions(String perm, int requestCode) {
        if (ContextCompat.checkSelfPermission(this, perm) != PackageManager.PERMISSION_GRANTED) {
            if (!ActivityCompat.shouldShowRequestPermissionRationale(this, perm)) {
                ActivityCompat.requestPermissions(this, new String[]{perm}, requestCode);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_FINE_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.i("KK", "onRequestPermissionsResult: Location permission granter");
                }
            }
        }
    }

    public void switchOnGPS() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(this)
                .setMessage("Please switch on GPS")
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(intent);
                    }
                })
                .setNegativeButton("SKIP", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        dialog.show();
    }
}