package com.kalkotekedar.www.weatherforecast.view.activity.home;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.kalkotekedar.www.weatherforecast.R;
import com.kalkotekedar.www.weatherforecast.view.activity.gps.SearchByGPS;
import com.kalkotekedar.www.weatherforecast.view.activity.search.CityNames;


public class HomeScreen extends AppCompatActivity {

    private Context context = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_screen);

        initViews();
    }

    private void initViews() {
        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mToolbar.setTitle("Example Forecast");
        mToolbar.setTitleTextColor(getResources().getColor(R.color.white));
        setSupportActionBar(mToolbar);
    }

    public void searchByGps(View view) {
        startActivity(new Intent(context, SearchByGPS.class));
    }

    public void searchByName(View view) {
        startActivity(new Intent(context, CityNames.class));
    }
}