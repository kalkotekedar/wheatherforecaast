package com.kalkotekedar.www.weatherforecast.view.Adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.kalkotekedar.www.weatherforecast.R;
import com.kalkotekedar.www.weatherforecast.utils.Constants;
import com.kalkotekedar.www.weatherforecast.view.activity.weather.WeatherList;

import java.util.ArrayList;

/**
 * Created by Kedar on 01-07-2016.
 */

public class CityAdapter extends BaseAdapter {
    ArrayList<String> cities;
    Context context;
    LayoutInflater inflater;

    public CityAdapter(Context context, ArrayList<String> cities) {
        this.cities = cities;
        this.context = context;
        inflater = LayoutInflater.from(this.context);
    }

    @Override
    public int getCount() {
        return cities.size();
    }

    @Override
    public Object getItem(int position) {
        return cities.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        MyViewHolder mViewHolder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.item_city, parent, false);
            mViewHolder = new MyViewHolder(convertView);
            convertView.setTag(mViewHolder);
        } else {
            mViewHolder = (MyViewHolder) convertView.getTag();
        }

        final String city = cities.get(position);

        mViewHolder.tvCityName.setText(city);
        mViewHolder.ivDeleteCity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cities.remove(city);
                notifyDataSetChanged();
            }
        });
        mViewHolder.cityContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i("KK", "onClick: " + cities.get(position));
                Intent seeWeather = new Intent(context, WeatherList.class);
                seeWeather.putExtra(Constants.CITY, cities.get(position));
                context.startActivity(seeWeather);
            }
        });
        return convertView;
    }

    public void addCity(String city) {
        cities.add(city);
        notifyDataSetChanged();
    }

    private class MyViewHolder {
        TextView tvCityName;
        ImageView ivDeleteCity;
        FrameLayout cityContainer;

        public MyViewHolder(View item) {
            cityContainer = (FrameLayout) item.findViewById(R.id.fl_city_container);
            tvCityName = (TextView) item.findViewById(R.id.tv_city_name);
            ivDeleteCity = (ImageView) item.findViewById(R.id.iv_close);
        }
    }
}