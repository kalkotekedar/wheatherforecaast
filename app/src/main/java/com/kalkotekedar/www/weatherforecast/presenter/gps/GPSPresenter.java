package com.kalkotekedar.www.weatherforecast.presenter.gps;

import android.app.ProgressDialog;
import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;
import com.kalkotekedar.www.weatherforecast.model.Example;
import com.kalkotekedar.www.weatherforecast.utils.Constants;
import com.kalkotekedar.www.weatherforecast.utils.URL;
import com.kalkotekedar.www.weatherforecast.view.activity.gps.ISearchByGPS;
import com.kalkotekedar.www.weatherforecast.view.activity.gps.SearchByGPS;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by Kedar on 01-07-2016.
 */
public class GPSPresenter implements IGPSPresenter {

    private Context context;
    ProgressDialog mProgressDialog;
    private ISearchByGPS iSearchByGPS;

    public GPSPresenter(SearchByGPS searchByGPS, Context context) {
        this.context = context;
        this.iSearchByGPS = searchByGPS;
    }

    @Override
    public void getCityName(Context context, Location location) {
        try {
            Geocoder gcd = new Geocoder(context, Locale.getDefault());
            List<Address> addresses = gcd.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
            if (addresses.size() > 0) {
                Log.i("KK", "getCityName: " + addresses.get(0).getLocality());
                new getCityForecast().execute(addresses.get(0).getLocality());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    class getCityForecast extends AsyncTask<String, Void, Void> {
        private Example example;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog = new ProgressDialog(context);
            mProgressDialog.setMessage("Getting updates please wait...");
            mProgressDialog.setCancelable(false);
            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(String... params) {
            try {
                OkHttpClient client = new OkHttpClient();

                HttpUrl.Builder urlBuilder = HttpUrl.parse(URL.WEATHER_URL).newBuilder();
                urlBuilder.addQueryParameter("q", params[0]);
                urlBuilder.addQueryParameter("cnt", "14");
                urlBuilder.addQueryParameter("APPID", Constants.WEATHER_API_KEY);
                String url = urlBuilder.build().toString();

                Request request = new Request.Builder()
                        .url(url)
                        .build();

                Response response = client.newCall(request).execute();
                Gson gson = new Gson();
                example = gson.fromJson(response.body().string(), Example.class);
                Log.i("KK", "doInBackground: " + example.getList().toString());
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (mProgressDialog.isShowing())
                mProgressDialog.dismiss();
            if (example != null)
                iSearchByGPS.displayForecast(example.getList(), example.getCity());
        }
    }
}