package com.kalkotekedar.www.weatherforecast.presenter.search;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;
import com.kalkotekedar.www.weatherforecast.model.Example;
import com.kalkotekedar.www.weatherforecast.utils.Constants;
import com.kalkotekedar.www.weatherforecast.utils.URL;
import com.kalkotekedar.www.weatherforecast.view.activity.weather.IWeatherList;

import java.io.IOException;

import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by Kedar on 28-06-2016.
 */
public class WeatherListPresenter implements IWeatherListPresenter {

    private Context context;
    ProgressDialog mProgressDialog;
    private IWeatherList iWeatherList;

    public WeatherListPresenter(IWeatherList iNameSearch, Context context) {
        this.context = context;
        this.iWeatherList = iNameSearch;
    }

    @Override
    public void getForecastByCity(String city) {
        new getCityForecast().execute(city);
    }

    class getCityForecast extends AsyncTask<String, Void, Void> {
        private Example example;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog = new ProgressDialog(context);
            mProgressDialog.setMessage("Getting updates please wait...");
            mProgressDialog.setCancelable(false);
            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(String... params) {
            try {
                OkHttpClient client = new OkHttpClient();

                HttpUrl.Builder urlBuilder = HttpUrl.parse(URL.WEATHER_URL).newBuilder();
                urlBuilder.addQueryParameter("q", params[0]);
                urlBuilder.addQueryParameter("cnt", "14");
                urlBuilder.addQueryParameter("APPID", Constants.WEATHER_API_KEY);
                String url = urlBuilder.build().toString();

                Request request = new Request.Builder()
                        .url(url)
                        .build();

                Response response = client.newCall(request).execute();
                Gson gson = new Gson();
                example = gson.fromJson(response.body().string(), Example.class);
                Log.i("KK", "doInBackground: " + example.getList().toString());
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (mProgressDialog.isShowing())
                mProgressDialog.dismiss();
            if (example != null)
                iWeatherList.displayForecast(example.getList(), example.getCity());
        }
    }
}