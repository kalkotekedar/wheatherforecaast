package com.kalkotekedar.www.weatherforecast.view.activity.search;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.kalkotekedar.www.weatherforecast.BaseActivity;
import com.kalkotekedar.www.weatherforecast.R;
import com.kalkotekedar.www.weatherforecast.utils.Utils;
import com.kalkotekedar.www.weatherforecast.view.Adapter.CityAdapter;

import java.util.ArrayList;

public class CityNames extends BaseActivity implements View.OnClickListener {

    private Context context = this;
    private EditText ET_getCityName;
    private CityAdapter adapter;
    private ArrayList<String> cities = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_name_search);
        init();
    }

    private void init() {
        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mToolbar.setTitle("City List");
        mToolbar.setTitleTextColor(getResources().getColor(R.color.white));
        setSupportActionBar(mToolbar);
        ET_getCityName = (EditText) findViewById(R.id.et_city_name);
        Button BT_searchForeCast = (Button) findViewById(R.id.bt_search);
        BT_searchForeCast.setOnClickListener(this);

        LinearLayoutManager layoutManager = new LinearLayoutManager(context);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        ListView lv_cityNames = (ListView) findViewById(R.id.rv_forecast);
        adapter = new CityAdapter(context, cities);
        lv_cityNames.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.bt_search:
                if (checkInternetConnection()) {
                    if (!ET_getCityName.getText().equals("")) {
                        adapter.addCity(ET_getCityName.getText().toString());
                        ET_getCityName.setText("");
                        Toast.makeText(CityNames.this, "City added successful", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(CityNames.this, "Enter City name please", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Utils.showSnackbar(v, "Please check your Internet Connection", Snackbar.LENGTH_INDEFINITE);
                }
                break;
        }
    }
}