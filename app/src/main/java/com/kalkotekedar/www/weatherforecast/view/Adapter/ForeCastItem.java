package com.kalkotekedar.www.weatherforecast.view.Adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.kalkotekedar.www.weatherforecast.R;

/**
 * Created by Kedar on 28-06-2016.
 */
public class ForeCastItem extends RecyclerView.ViewHolder {

    public TextView timeStamp;
    public TextView day;
    public TextView night;
    public TextView morn;
    public TextView eve;
    public TextView min;
    public TextView max;
    public TextView pressure;
    public TextView humidity;
    public TextView speed;
    public TextView deg;
    public TextView cloud;
    public TextView rain;
    public TextView main;
    public TextView description;


    public ForeCastItem(View itemView) {
        super(itemView);
        timeStamp = (TextView) itemView.findViewById(R.id.tv_time_stamp);
        day = (TextView) itemView.findViewById(R.id.tv_day);
        night = (TextView) itemView.findViewById(R.id.tv_night);
        min = (TextView) itemView.findViewById(R.id.tv_min);
        max = (TextView) itemView.findViewById(R.id.tv_max);
        morn = (TextView) itemView.findViewById(R.id.tv_morn);
        eve = (TextView) itemView.findViewById(R.id.tv_eve);
        pressure = (TextView) itemView.findViewById(R.id.tv_pressure);
        humidity = (TextView) itemView.findViewById(R.id.tv_humidity);
        speed = (TextView) itemView.findViewById(R.id.tv_speed);
        deg = (TextView) itemView.findViewById(R.id.tv_deg);
        cloud = (TextView) itemView.findViewById(R.id.tv_cloud);
        rain = (TextView) itemView.findViewById(R.id.tv_rain);
        main = (TextView) itemView.findViewById(R.id.tv_main);
        description = (TextView) itemView.findViewById(R.id.tv_description);
    }
}