package com.kalkotekedar.www.weatherforecast.presenter.gps;

import android.content.Context;
import android.location.Location;

/**
 * Created by Kedar on 01-07-2016.
 */
public interface IGPSPresenter {
    void getCityName(Context context, Location location);
}