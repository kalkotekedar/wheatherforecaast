package com.kalkotekedar.www.weatherforecast.view.activity.weather;


import com.kalkotekedar.www.weatherforecast.model.City;
import com.kalkotekedar.www.weatherforecast.model.List;

/**
 * Created by Kedar on 28-06-2016.
 */
public interface IWeatherList {
    void displayForecast(java.util.List<List> list, City city);
}