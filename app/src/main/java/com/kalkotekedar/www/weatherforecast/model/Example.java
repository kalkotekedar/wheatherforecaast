package com.kalkotekedar.www.weatherforecast.model;

import java.util.ArrayList;

/**
 * Created by Kedar on 27-06-2016.
 */

public class Example {
    City city;
    int cod;
    double message;
    int count;
    java.util.List<List> list = new ArrayList<List>();

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public int getCod() {
        return cod;
    }

    public void setCod(int cod) {
        this.cod = cod;
    }

    public double getMessage() {
        return message;
    }

    public void setMessage(double message) {
        this.message = message;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public java.util.List getList() {
        return list;
    }

    public void setList(java.util.List list) {
        this.list = list;
    }

    @Override
    public String toString() {
        return "Example{" +
                "city=" + city +
                ", cod=" + cod +
                ", message=" + message +
                ", count=" + count +
                ", list=" + list +
                '}';
    }
}